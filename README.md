# Building your resume with Docker Containers, LaTeX, Ruby, and Sinatra
### requirements: Docker - full internet access, Ruby - Version 2.2.2


**commands to create a new pdf from this repo**
  - clone the base repo for your changes
* `git clone https://gitlab.com/sinajames/resume.git`
  - change the resume specific files for your resume, make note of the stringent formatting of yaml for successful recreation
*  `located at the current working directory in folder resume-configuration`
  - run the resume creator after changes to the resume have been made
* `ruby resume-converter.rb`
  - convert the created .tex file to a PDF
* `docker run --rm -t -v ${PWD}:/source schickling/latex pdflatex resume.tex`
  - build the docker container which will hold the new resume.pdf file and host it with sinatra
* `docker build -t 'REPLACE-WITH-PROJECT-NAME!'/resume .`
  - run the new container on port 80 local to the container-host
* `docker run --rm -p 80:80 'REPLACE-WITH-PROJECT-NAME!'/resume`


[Current Example CI/CD Pipeline] (https://gitlab.com/sinajames/resume/pipelines)