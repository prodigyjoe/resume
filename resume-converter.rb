#!/usr/bin/env ruby

require 'yaml'

$page_format = YAML.load_file('./resume-configuration/page_format.yaml')
$skills      = YAML.load_file('./resume-configuration/skills.yaml')
$candidate   = YAML.load_file('./resume-configuration/candidate.yaml')
$resume      = YAML.load_file('./resume-configuration/resume.yaml')
$education   = YAML.load_file('./resume-configuration/education.yaml')

class DocumentSpecs
  attr_accessor :fontsize, :papersize, :font, :theme

  def initialize
    self.fontsize  = $page_format['page_format']['fontsize']
    self.papersize = $page_format['page_format']['papersize']
    self.font      = $page_format['page_format']['font']
    self.theme     = $page_format['page_format']['theme']
  end

  def validate_document_specs
    self.instance_variables.map do |x|
      if self.instance_variable_get(x).empty? == true
        puts "#{x.to_s} is missing from the object \n"
      end
    end
  end

  def generate_document_specs
    <<eot
%--- Document Specs ---
\\documentclass[#{self.fontsize},#{self.papersize},#{self.font}]{#{self.theme}}
%--- ---

eot
  end

end

class ThemeProperties
  attr_accessor :cvstyle, :color, :package

  def initialize
    self.cvstyle = $page_format['cvstyle']
    self.color   = $page_format['cvcolor']
    self.package = $page_format['package']
  end

  def generate_style
    "\\moderncvstyle{#{self.cvstyle}}"
  end

  def generate_color
    "\\moderncvcolor{#{self.color}}"
  end

  def generate_package
    "\\usepackage{#{self.package}}"
  end

  def validate_theme_properties
    self.instance_variables.map do |x|
      if self.instance_variable_get(x).empty? == true
        puts "#{x.to_s} is missing from the object \n"
      end
    end
  end

  def generate_theme_properties
    <<eot
%--- Theme Properties ---
#{self.generate_style}
#{self.generate_color}
#{self.generate_package}
%--- ---

eot
  end

end

class PageFormat
  attr_accessor :type, :input, :size, :shape, :width

  def initialize
    self.type  = $page_format['package_format']['encoding']
    self.input = $page_format['package_format']['type']
    self.size  = $page_format['package_size']['scale']
    self.shape = $page_format['package_size']['type']
    self.width = $page_format['column_width']
  end

  def generate_encoding
    "\\usepackage[#{self.type}]{#{self.input}}"
  end

  def generate_geometry
    "\\usepackage[scale=#{self.size}]{#{self.shape}}"
  end

  def generate_hintscolumn
    "\\setlength{\\hintscolumnwidth}{#{self.width}}"
  end

  def validate_page_format
    self.instance_variables.map do |x|
      if self.instance_variable_get(x).empty? == true
        puts "#{x.to_s} is missing from the object \n"
      end
    end
  end

  def generate_page_format
    <<eot
%--- Page Formatting ---
#{self.generate_encoding}
#{self.generate_geometry}
#{self.generate_hintscolumn}
%--- ---

eot
  end

end

class CandidateProfile
  attr_accessor :first_name, :last_name, :profession, :email, :homepage, :address, :cell, :zip

  def initialize
    self.first_name = $candidate['first_name']
    self.last_name  = $candidate['last_name']
    self.profession = $candidate['title']
    self.email      = $candidate['email']
    self.address    = $candidate['address']
    self.cell       = $candidate['cell']
    self.homepage   = $candidate['homepage']
    self.zip        = $candidate['zip']
  end

  def generate_name
    "\\firstname{#{self.first_name}}"
  end

  def generate_last_name
    "\\familyname{#{self.last_name}}"
  end

  def generate_profession
    "\\title{#{self.profession}}"
  end

  def generate_email
    "\\email{#{self.email}}"
  end

  def generate_homepage
    "\\homepage{#{self.homepage}}"
  end

  def generate_address
    "\\address{#{self.address}}{#{self.zip}}"
  end

  def generate_cell
    "\\mobile{#{self.cell}}"
  end

  def begin_document
    "\\begin{document}\n\\maketitle"
  end

  def validate_candidate_profile
    self.instance_variables.map do |x|
      if self.instance_variable_get(x).empty? == true
        puts "#{x.to_s} is missing from the object \n"
      end
    end
  end

  def generate_candidate_profile
    <<eot
%--- Candidate Details ---
#{self.generate_name}
#{self.generate_last_name}
#{self.generate_profession}
#{self.generate_address}
#{self.generate_cell}
#{self.generate_email}
#{self.generate_homepage}
%--- ---

#{self.begin_document}

eot
  end

end

class CandidateSkills
  attr_accessor :section, :header, :skills

  def initialize
    self.section = $skills['skills']
    self.header  = $skills['skills_section_header']
    self.skills  = ""
  end

  def generate_header
    "\\section{#{self.header}}"
  end

  def aggregate_candidate_skills
    self.section.map do |x|
      self.skills << "\n\\cvitem{#{x['section']}}{#{x['skills']*", "}}"
    end
  end

  def generate_candidate_skills
    self.aggregate_candidate_skills
    <<eot
%--- Skills Section ---
#{self.generate_header}
#{self.skills}
%--- ---

eot
  end

end

class CandidateExperience
  attr_accessor :experience_section_header, :job, :items

  def initialize
    @experience_section_header = $resume['experience_section_header']
    @job                       = $resume['experience']
    @items                     = ""
  end

  def generate_header
    "\\section{#{self.experience_section_header}}"
  end

  def validate_candidate_experience
    self.instance_variables.map do |x|
      if self.instance_variable_get(x).empty? == true
        puts "#{x.to_s} is missing from the object \n"
      end
    end
  end

  def aggregate_experience_section
    self.job.map do |x|
      self.items << "\n%--- #{x['company']} ---"
      self.items << "\n\\cventry{#{x['timeframe']}}{#{x['position']}}{#{x['company']}}{#{x['location']}}{}{#{x['company_description']}\n"
      self.items << "\\begin{itemize}\n"
      x['experience'].map do |t|
        self.items << "   \\item #{t['proficiency']}\n"
        self.items << "   \\begin{itemize}\n"
        t['examples'].map do |b|
          self.items << "     \\item #{b}\n"
        end
        self.items << "   \\end{itemize}\n"
      end
      self.items << " \\end{itemize}\n}\n"
      self.items << "%--- ---\n"
    end
  end

  def generate_experience_section
    self.aggregate_experience_section
      <<eot
%--- Experience Section ---
#{self.generate_header}
#{self.items}

eot
  end

end

class CandidateEducation
  attr_accessor :education_section, :education_header, :education_items

  def initialize
    self.education_section = $education['education_section']
    self.education_header  = $education['education_section_header']
    self.education_items   = ""
  end

  def generate_header
    "\\section{#{self.education_header}}"
  end

  def aggregate_education_section
    self.education_section.map do |x|
      self.education_items << "\n\\cventry{#{x['timeframe']}}{#{x['organization']}}{#{x['location']}}{#{x['major_and_gpa']}}{}{#{x['extra_info']}}"
    end
  end

  def generate_education_section
    self.aggregate_education_section
      <<eot
%--- Education Section ---
#{self.generate_header}
#{self.education_items}
%--- ---

\\end{document}
eot
  end

end

File.open('resume.tex', 'w+') do |w|
  a = DocumentSpecs.new
  w.write(a.generate_document_specs)

  b = ThemeProperties.new
  w.write(b.generate_theme_properties)

  c = PageFormat.new
  w.write(c.generate_page_format)

  d = CandidateProfile.new
  w.write(d.generate_candidate_profile)

  e = CandidateSkills.new
  w.write(e.generate_candidate_skills)

  f = CandidateExperience.new
  w.write(f.generate_experience_section)

  g = CandidateEducation.new
  w.write(g.generate_education_section)
end
